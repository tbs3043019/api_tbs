<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class SubscriptionTest extends ApiTestCase
{
    /***
     * Test GET subscription api
     *
     */
    public function testGetSubscription(): void
    {
        // Test GET OK
        static::createClient()->request('GET', '/api/subscription/1');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');

        // Test GET KO
        static::createClient()->request('GET', '/api/subscription/10000');
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonEquals(["not found with id 10000"]);
    } // testGetSubscription

    /***
     * Test POST subscription api
     *
     */
    public function testPostSubscription(): void
    {
        // Test POST OK
        static::createClient()->request(
            'POST',
            '/api/subscription',
            ['headers' => ['Accept' => 'application/json'],
                'json' => [
                    "contact" => [
                        "name" => "titi" . rand(1,10000),
                        "firstname" => "toto" . rand(1,10000)
                    ],
                    "product" => [
                        "label" => "mon label" .  rand(1,10000)
                    ]
                ]]);
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');


        // Test POST KO
        static::createClient()->request(
            'POST',
            '/api/subscription',
            ['headers' => ['Accept' => 'application/json'],
                'json' => [
                    "contact" => [
                        "name" => "titi" . rand(1,10000)
                    ],
                    "product" => [
                        "label" => "mon label" .  rand(1,10000)
                    ]
                ]]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains(["Firstname missing"]);
        $this->assertResponseHeaderSame('content-type', 'application/json');
    } // testPostSubscription

    /***
     * Test PUT subscription api
     *
     */
    public function testPutSubscription(): void
    {
        // Test PUT OK
        static::createClient()->request(
            'PUT',
            '/api/subscription/2',
            ['headers' => ['Accept' => 'application/json'],
                'json' => [
                    "contact" => [
                        "name" => "titi" . rand(1,10000),
                        "firstname" => "toto" . rand(1,10000)
                    ],
                    "product" => [
                        "label" => "mon label" .  rand(1,10000)
                    ]
                ]]);
        $this->assertResponseStatusCodeSame(204);
        $this->assertResponseIsSuccessful();

        // Test PUT KO
        static::createClient()->request(
            'PUT',
            '/api/subscription/2',
            ['headers' => ['Accept' => 'application/json'],
                'json' => [
                    "contact" => [
                        "name" => "titi" . rand(1,10000),
                    ],
                    "product" => [
                        "label" => "mon label" .  rand(1,10000)
                    ]
                ]]);
        $this->assertResponseStatusCodeSame(400);
        $this->assertJsonContains(["Firstname missing"]);
        $this->assertResponseHeaderSame('content-type', 'application/json');
    } // testPutSubscription

    /***
     * Test DELETE subscription api
     *
     */
    public function testDeleteSubscription(): void
    {
        // Test DELETE KO
        static::createClient()->request('DELETE', '/api/subscription/10000');
        $this->assertResponseStatusCodeSame(404);
        $this->assertJsonContains(["status" => 404,"message" => "Object not found"]);
    } // testDeleteSubscription

}
