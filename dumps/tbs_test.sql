CREATE DATABASE  IF NOT EXISTS `tbs_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tbs_test`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: tbs_test
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Schmidt','Jewell'),(2,'Powlowski','Ayden'),(3,'Deckow','Jessy'),(4,'Borer','Bertrand'),(5,'Berge','Amari'),(6,'Emmerich','Tremayne'),(7,'Bartell','Annie'),(8,'Hagenes','Roosevelt'),(9,'Schuster','Kadin'),(10,'Feest','Lucius'),(11,'Senger','Ayana'),(12,'Wilderman','Matilda'),(13,'Bergnaum','Danyka'),(14,'Williamson','Ozella'),(15,'Mann','Asha'),(16,'Blanda','Micah'),(17,'Doyle','Adrianna'),(18,'Orn','Dereck'),(19,'Lindgren','Connie'),(20,'Hartmann','Kamren'),(21,'Roob','Clyde'),(22,'Waters','Virgil'),(23,'Greenfelder','Connor'),(24,'Little','Maybell'),(25,'Considine','Elody'),(26,'Schaefer','Camila'),(27,'Grady','Jonathon'),(28,'Gaylord','Arlene'),(29,'Johnston','Darien'),(30,'Heathcote','Derek'),(31,'Mills','Aimee'),(32,'Walker','Loy'),(33,'O\'Connell','Romaine'),(34,'Rosenbaum','Audreanne'),(35,'Cartwright','Kathlyn'),(36,'Sanford','Nya'),(37,'Farrell','Darwin'),(38,'Emmerich','Nannie'),(39,'Smith','Haven'),(40,'Becker','Eliezer'),(41,'Runolfsson','Beulah'),(42,'Toy','Robbie'),(43,'Zemlak','Heather'),(44,'Wiegand','Cecil'),(45,'Durgan','Unique'),(46,'O\'Keefe','Linda'),(47,'Cremin','Keely'),(48,'Ward','Richard'),(49,'Morissette','Emmett'),(50,'Kiehn','Keely'),(51,'titi','toto'),(52,'titi','toto'),(53,'titi','toto'),(54,'titi','toto'),(55,'titi','toto'),(56,'titi','toto'),(57,'titi','toto'),(58,'titi','toto'),(59,'titi','toto'),(60,'titi','toto'),(61,'titi','toto'),(62,'titi5926','toto4736'),(63,'titi2851','toto2907'),(64,'titi','toto'),(65,'titi258','toto1573'),(66,'titi364','toto1598'),(67,'titi','toto'),(68,'titi4875','toto5135'),(69,'titi357','toto5565'),(70,'titi3236','toto1594'),(71,'titi4839','toto4551'),(72,'titi1837','toto5512'),(73,'titi8289','toto2105'),(74,'titi8462','toto8303'),(75,'titi2815','toto372'),(76,'titi4066','toto9522'),(77,'titi9093','toto650'),(78,'titi236','toto3744'),(79,'titi1303','toto6206'),(80,'titi6362','toto627'),(81,'titi4132','toto4960'),(82,'titi2396','toto8570'),(83,'titi2862','toto6234'),(84,'titi5261','toto2178'),(85,'titi1439','toto4946'),(86,'titi1697','toto917'),(87,'titi9496','toto5086'),(88,'titi9326','toto8817'),(89,'titi8241','toto917');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20240508103608','2024-05-09 14:05:37',239);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'product 1'),(2,'product 2'),(3,'product 3'),(4,'product 4'),(5,'product 5'),(6,'product 6'),(7,'product 7'),(8,'product 8'),(9,'product 9'),(10,'product 10'),(11,'product 11'),(12,'product 12'),(13,'product 13'),(14,'product 14'),(15,'product 15'),(16,'product 16'),(17,'product 17'),(18,'product 18'),(19,'product 19'),(20,'product 20'),(21,'mon label'),(22,'mon label'),(23,'mon label'),(24,'mon label'),(25,'mon label'),(26,'mon label'),(27,'mon label'),(28,'mon label'),(29,'mon label'),(30,'mon label'),(31,'mon label'),(32,'mon label8268'),(33,'mon label8521'),(34,'mon label'),(35,'mon label4539'),(36,'mon label776'),(37,'mon label'),(38,'mon label6346'),(39,'mon label4824'),(40,'mon label9067'),(41,'mon label7823'),(42,'mon label5454'),(43,'mon label6168'),(44,'mon label1527'),(45,'mon label308'),(46,'mon label7102'),(47,'mon label4974'),(48,'mon label368'),(49,'mon label4993'),(50,'mon label4607'),(51,'mon label4978'),(52,'mon label3330'),(53,'mon label8309'),(54,'mon label5763'),(55,'mon label321'),(56,'mon label1982'),(57,'mon label2063'),(58,'mon label8419'),(59,'mon label3677');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contact_id` int NOT NULL,
  `product_id` int NOT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A3C664D3E7A1254A` (`contact_id`),
  KEY `IDX_A3C664D34584665A` (`product_id`),
  CONSTRAINT `FK_A3C664D34584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_A3C664D3E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (1,1,1,'2024-05-08 15:00:00',NULL),(2,89,59,'2024-05-08 15:00:00',NULL),(4,3,2,'2024-05-08 15:00:00',NULL),(5,51,21,'2024-05-09 14:57:50',NULL),(6,52,22,'2024-05-09 15:04:51',NULL),(7,53,23,'2024-05-09 15:05:04',NULL),(8,54,24,'2024-05-09 15:07:33',NULL),(9,55,25,'2024-05-09 15:07:44',NULL),(10,56,26,'2024-05-09 15:07:59',NULL),(11,57,27,'2024-05-09 15:10:15',NULL),(12,58,28,'2024-05-09 15:10:35',NULL),(13,59,29,'2024-05-09 15:10:41',NULL),(14,60,30,'2024-05-09 15:10:58',NULL),(16,62,32,'2024-05-09 15:21:01',NULL),(17,64,34,'2024-05-09 15:22:26',NULL),(18,65,35,'2024-05-09 15:22:26',NULL),(19,67,37,'2024-05-09 15:22:55',NULL),(20,68,38,'2024-05-09 15:22:55',NULL),(21,70,40,'2024-05-09 15:24:36',NULL),(22,71,41,'2024-05-09 15:24:36',NULL),(23,73,43,'2024-05-09 15:26:32',NULL),(24,74,44,'2024-05-09 15:26:32',NULL),(25,76,46,'2024-05-09 15:27:27',NULL),(26,78,48,'2024-05-09 15:27:31',NULL),(27,80,50,'2024-05-09 15:30:07',NULL),(28,82,52,'2024-05-09 15:30:26',NULL),(29,84,54,'2024-05-09 15:31:05',NULL),(30,86,56,'2024-05-09 15:31:18',NULL),(31,88,58,'2024-05-09 15:32:41',NULL);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-10 10:29:42
