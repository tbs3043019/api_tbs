CREATE DATABASE  IF NOT EXISTS `tbs` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tbs`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: tbs
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Hane','Kaylie'),(2,'Hessel','Darby'),(3,'Gleason','Ozella'),(4,'Rohan','Susie'),(5,'Kunde','Ashlee'),(6,'Eichmann','Roselyn'),(7,'Wolff','Ashly'),(8,'Collier','Jo'),(9,'Hintz','Tia'),(10,'Schaden','Fae'),(11,'Bartoletti','Katherine'),(12,'Heathcote','Romaine'),(13,'Roberts','Forest'),(14,'Moore','Brock'),(15,'Ankunding','Timmothy'),(16,'Lehner','Eliza'),(17,'Abbott','Moises'),(18,'Skiles','Eleonore'),(19,'Kulas','Tito'),(20,'Kris','Arely'),(21,'Lesch','Jefferey'),(22,'Ward','Cecil'),(23,'Schroeder','Brice'),(24,'Botsford','Abby'),(25,'Jast','Adah'),(26,'Bergnaum','Arnaldo'),(27,'Halvorson','Mekhi'),(28,'Marquardt','Amina'),(29,'Krajcik','Pascale'),(30,'Connelly','Bryana'),(31,'Bartoletti','Magali'),(32,'Borer','Mariah'),(33,'Bergnaum','Orval'),(34,'Shields','Meagan'),(35,'Tromp','Suzanne'),(36,'Cronin','Adolphus'),(37,'Rohan','Josianne'),(38,'Renner','Bennie'),(39,'Hoppe','Gertrude'),(40,'Braun','Afton'),(41,'Farrell','Abbey'),(42,'Gibson','Beth'),(43,'Torphy','Kariane'),(44,'Reichel','Catalina'),(45,'Mueller','Julianne'),(46,'O\'Keefe','Moriah'),(47,'Carroll','Rodrick'),(48,'Murray','Blair'),(49,'Witting','Janiya'),(50,'Kohler','Kenneth'),(101,'titi','toto'),(102,'titi','toto'),(103,'titi','toto'),(104,'titi','toto'),(105,'titi','toto'),(106,'titi','toto'),(108,'t1411','t1411'),(109,'t1411','t1411'),(110,'titi','toto'),(111,'titi','toto'),(112,'titi','toto'),(113,'titi','toto'),(114,'titi','toto');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20240508103608','2024-05-08 10:37:03',214);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'product 1'),(2,'product 2'),(3,'product 3'),(4,'product 4'),(5,'product 5'),(6,'product 6'),(7,'product 7'),(8,'product 8'),(9,'product 9'),(10,'product 10'),(11,'product 11'),(12,'product 12'),(13,'product 13'),(14,'product 14'),(15,'product 15'),(16,'product 16'),(17,'product 17'),(18,'product 18'),(19,'product 19'),(20,'product 20'),(21,'mon label'),(22,'mon label'),(23,'mon label'),(24,'mon label'),(25,'mon label'),(26,'mon label'),(27,'m1411'),(28,'m1411'),(29,'mon label'),(30,'mon label'),(31,'mon label'),(32,'insurance'),(33,'insurance');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contact_id` int NOT NULL,
  `product_id` int NOT NULL,
  `begin_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A3C664D3E7A1254A` (`contact_id`),
  KEY `IDX_A3C664D34584665A` (`product_id`),
  CONSTRAINT `FK_A3C664D34584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_A3C664D3E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` VALUES (1,112,31,'2024-05-08 15:00:00',NULL),(2,2,2,'2024-05-08 15:00:00',NULL),(4,3,2,'2024-05-08 15:00:00',NULL),(6,114,33,'2024-05-09 08:47:07',NULL),(7,102,22,'2024-05-09 09:08:07',NULL),(8,103,23,'2024-05-09 09:08:31',NULL),(9,104,24,'2024-05-09 09:08:49',NULL),(10,105,25,'2024-05-09 09:09:18',NULL),(11,106,26,'2024-05-09 09:09:50',NULL),(12,110,29,'2024-05-09 15:00:31',NULL),(13,113,32,'2024-05-09 20:19:21',NULL);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-10 10:27:29
