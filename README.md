#Api

You can test the api with nelmio api bundle here : 
http://localhost:8000/api/doc

Or you can use postman for example :
https://www.postman.com

I used both.

You can find dump sql in folder : 
dumps
For the test Contact name and firstname and product could be not unique, but you can also add key constraints for reel project
(ADD UNIQUE INDEX `uk_sub_name_firstname` (`name` ASC, `firstname` ASC) )

#Git

I got a problem with my repo git in local not synchronized with git lab with source tree.
You can find a screenshoot of history (sourcetree) in folder dumps/git
