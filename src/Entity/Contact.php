<?php

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["get_sub"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["get_sub"])]
    #[Assert\NotBlank(message: "Name missing")]
    #[Assert\Length(min: 2, max: 50, minMessage: "Name must be at least {{ limit }} characters", maxMessage: "Name must be at max {{ limit }} characters")]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(["get_sub"])]
    #[Assert\NotBlank(message: "Firstname missing")]
    #[Assert\Length(min: 2, max: 50, minMessage: "Firstname must be at least {{ limit }} characters", maxMessage: "Firstname must be at max {{ limit }} characters")]
    private ?string $firstname = null;

    /**
     * @var Collection<int, Subscription>
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'contact')]
    private Collection $subscriptions;

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setContact($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getContact() === $this) {
                $subscription->setContact(null);
            }
        }

        return $this;
    }
}
