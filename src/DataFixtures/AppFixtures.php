<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Fake data for entity Contact
        for ($i = 1; $i <= 50; $i++) {
            $faker = Faker\Factory::create();
            $contact = new Contact();
            $contact->setName($faker->lastName);
            $contact->setFirstname($faker->firstName);
            $manager->persist($contact);
        }
        $manager->flush();

        // Fake data for entity Product
        for ($i = 1; $i <= 20; $i++) {
            $product = new Product();
            $product->setLabel('product ' . $i);
            $manager->persist($product);
        }
        $manager->flush();
    }
}
