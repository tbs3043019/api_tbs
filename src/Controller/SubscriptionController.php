<?php

namespace App\Controller;

use App\Entity\Subscription;
use App\Repository\SubscriptionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Attributes as OA;

#[Route('/api')]
class SubscriptionController extends AbstractController
{
    /**
     *  Api to get detail of subscription (needed id of contact! not id of subscription)
     *
     * @param int $idContact id of contact to search
     * @param SubscriptionRepository $subRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/subscription/{idContact}', name: 'api_get_subscription', methods: ['GET'])]
    public function getSubscription(
        int $idContact,
        SubscriptionRepository $subRepository,
        SerializerInterface $serializer): JsonResponse
    {
        $subscriptions = $subRepository->findBy(['contact' => $idContact]);
        if ($subscriptions) { // only if exists
            $jsonSub = $serializer->serialize($subscriptions, 'json', ['groups' => 'get_sub']);
            return new JsonResponse($jsonSub, Response::HTTP_OK, [], true);
        }

        return new JsonResponse(["not found with id $idContact"], Response::HTTP_NOT_FOUND);
    } // getSubscription

    /**
     *  Api to create new subscription
     *
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(
            type: Subscription::class,
            example: [
                "contact" => [
                    "name" => "titi",
                    "firstname" => "toto"
                ],
                "product" => [
                    "label" => "insurance"
                ]
            ]
        )
    )]
    #[Route('/subscription', name: 'api_post_subscription', methods: ['POST'])]
    public function postSubscription(
        Request $request,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator): JsonResponse
    {
        if (empty($request->getContent())) {
            return new JsonResponse('Please send valid Json', Response::HTTP_BAD_REQUEST);
        }
        /** @var Subscription $subscription */
        try {
            $subscription = $serializer->deserialize($request->getContent(), Subscription::class, 'json');
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        if (empty($subscription->getContact()) || empty($subscription->getProduct())) {
            return new JsonResponse(['Please send valid Json with contact data and person data'], Response::HTTP_BAD_REQUEST);
        }
        // Checks errors
        $detailErrors = [];
        $errorsContact = $validator->validate($subscription->getContact());
        $errorsProduct = $validator->validate($subscription->getProduct());
        if ($errorsContact->count() > 0) {
            foreach ($errorsContact as $constraintViolation) {
                $detailErrors[] = $constraintViolation->getMessage();
            }
        }
        if ($errorsProduct->count() > 0) {
            foreach ($errorsProduct as $constraintViolation) {
                $detailErrors[] = $constraintViolation->getMessage();
            }
        }
        if (!empty($detailErrors)) {
            return new JsonResponse($detailErrors, JsonResponse::HTTP_BAD_REQUEST);
        }
        // persist and flush data
        $entityManager->persist($subscription->getContact());
        $entityManager->persist($subscription->getProduct());
        $entityManager->persist($subscription);
        $entityManager->flush();

        $jsonSub = $serializer->serialize($subscription, 'json', ['groups' => 'get_sub']);

        return new JsonResponse($jsonSub, Response::HTTP_CREATED, [], true);
    } // postSubscription


    /**
     *  Api to update subscription
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[OA\RequestBody(
        required: true,
        content: new OA\JsonContent(
            type: Subscription::class,
            example: [
                "contact" => [
                    "name" => "titi",
                    "firstname" => "toto"
                ],
                "product" => [
                    "label" => "insurance"
                ]
            ]
        )
    )]
    #[Route('/subscription/{id}', name: 'api_put_subscription', methods: ['PUT'])]
    public function putSubscription(
        Request $request,
        Subscription $sub,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ValidatorInterface $validator): JsonResponse
    {
        if (empty($request->getContent())) {
            return new JsonResponse(['Please send valid Json'], Response::HTTP_BAD_REQUEST);
        }
        /** @var Subscription $subscription */
        try {
            $subscription = $serializer->deserialize(
                $request->getContent(),
                Subscription::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $sub] // pass sub from query url
            );
        } catch (\Exception $exception) {
            return new JsonResponse([$exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
        if (empty($subscription->getContact()) || empty($subscription->getProduct())) {
            return new JsonResponse(['Please send valid Json with contact data and person data'], Response::HTTP_BAD_REQUEST);
        }
        // Checks errors
        $detailErrors = [];
        $errorsContact = $validator->validate($subscription->getContact());
        $errorsProduct = $validator->validate($subscription->getProduct());
        if ($errorsContact->count() > 0) {
            foreach ($errorsContact as $constraintViolation) {
                $detailErrors[] = $constraintViolation->getMessage();
            }
        }
        if ($errorsProduct->count() > 0) {
            foreach ($errorsProduct as $constraintViolation) {
                $detailErrors[] = $constraintViolation->getMessage();
            }
        }
        if (!empty($detailErrors)) {
            return new JsonResponse($detailErrors, JsonResponse::HTTP_BAD_REQUEST);
        }
        // persist and flush data
        $entityManager->persist($subscription->getContact());
        $entityManager->persist($subscription->getProduct());
        $entityManager->persist($subscription);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    } // putSubscription

    /**
     * Api to delete one subscription
     *
     * @param Subscription $sub object sub to delete
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/subscription/{id}', name: 'api_delete_subscription', methods: ['DELETE'])]
    public function deleteSubscription(Subscription $sub, EntityManagerInterface $entityManager): JsonResponse
    {
        $entityManager->remove($sub);
        $entityManager->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    } // deleteSubscription
}
